<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2023-11-16 14:06:18 by Dominik Domagała-->
<display version="2.0.0">
  <name>Load</name>
  <width>990</width>
  <height>350</height>
  <widget type="label" version="2.0.0">
    <name>LB_LoadConnectionStatus</name>
    <text>Load Connection Status</text>
    <x>20</x>
    <y>20</y>
    <width>185</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_OutputRelayStatus</name>
    <text>Output Relay Status</text>
    <x>285</x>
    <y>20</y>
    <width>157</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_PSUPowerOverview</name>
    <text>PSU Power Overview</text>
    <x>490</x>
    <y>20</y>
    <width>163</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_LoadConnectionCommands</name>
    <text>Load Connection Commands</text>
    <x>30</x>
    <y>151</y>
    <width>221</width>
    <height>26</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_SingleLoadMode</name>
    <text>Single Load Mode</text>
    <x>100</x>
    <y>60</y>
    <width>119</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_DoubleLoadMode</name>
    <text>Double Load Mode</text>
    <x>100</x>
    <y>100</y>
    <width>126</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_SingleLoad</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_SingleLoad</pv_name>
    <x>60</x>
    <y>60</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for one heater (current setup)</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_DoubleLoad</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DoubleLoad</pv_name>
    <x>60</x>
    <y>100</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <tooltip>One card for two heaters (current setup)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_DoubleLoad</name>
    <actions execute_as_one="true">
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>WritePV</description>
      </action>
    </actions>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:Cmd_DoubleLoad</pv_name>
    <text>Double Load Command</text>
    <x>60</x>
    <y>290</y>
    <width>200</width>
    <height>33</height>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Both Heaters</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Heater1Only</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:Cmd_Relay1</pv_name>
    <text>Heater 1 Only</text>
    <x>60</x>
    <y>200</y>
    <width>201</width>
    <height>33</height>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Only Main Heater</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Heater2Only</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:Cmd_Relay2</pv_name>
    <text>Heater 2 Only</text>
    <x>60</x>
    <y>230</y>
    <width>201</width>
    <height>33</height>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Only Slave Heater</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_Relay#1</name>
    <text>Relay #1</text>
    <x>325</x>
    <y>60</y>
    <width>56</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_Relay#2</name>
    <text>Relay #2</text>
    <x>325</x>
    <y>100</y>
    <width>56</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LB_Relay#1</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_Relay1</pv_name>
    <x>285</x>
    <y>60</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Main Heater</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LB_Relay#2</name>
    <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_Relay2</pv_name>
    <x>285</x>
    <y>100</y>
    <width>25</width>
    <height>25</height>
    <off_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </off_color>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Slave heater</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_MainHeater</name>
    <text>${Dev}-${MainHeaterIndex}</text>
    <x>490</x>
    <y>60</y>
    <width>181</width>
    <height>23</height>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Main Heater Name</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_SlaveHeater</name>
    <text>${Dev}-${SlaveHeaterIndex}</text>
    <x>490</x>
    <y>100</y>
    <width>185</width>
    <height>23</height>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Slave Heater Name</tooltip>
  </widget>
  <widget type="progressbar" version="2.0.0">
    <name>MET_HeaterPower_Main</name>
    <pv_name>${SecSub}:${WIDDis}-${Dev}-${MainHeaterIndex}:HeaterPower</pv_name>
    <x>580</x>
    <y>60</y>
    <width>219</width>
    <height>26</height>
    <rules>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <limits_from_pv>false</limits_from_pv>
    <maximum>50.0</maximum>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_HeaterPower_Main</name>
    <pv_name>${SecSub}:${WIDDis}-${Dev}-${MainHeaterIndex}:HeaterPower</pv_name>
    <x>590</x>
    <y>60</y>
    <width>219</width>
    <height>30</height>
    <background_color>
      <color red="255" green="254" blue="253">
      </color>
    </background_color>
    <transparent>true</transparent>
    <precision>3</precision>
    <rules>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Calculated Heater Power</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="progressbar" version="2.0.0">
    <name>MET_HeaterPower_Main_1</name>
    <pv_name>${SecSub}:${WIDDis}-${Dev}-${SlaveHeaterIndex}:HeaterPower</pv_name>
    <x>580</x>
    <y>100</y>
    <width>219</width>
    <height>26</height>
    <rules>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip></tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <limits_from_pv>false</limits_from_pv>
    <maximum>50.0</maximum>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_HeaterPower_Slave</name>
    <pv_name>${SecSub}:${WIDDis}-${Dev}-${SlaveHeaterIndex}:HeaterPower</pv_name>
    <x>590</x>
    <y>100</y>
    <width>219</width>
    <height>30</height>
    <background_color>
      <color red="255" green="254" blue="253">
      </color>
    </background_color>
    <transparent>true</transparent>
    <precision>3</precision>
    <rules>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
    <tooltip>Calculated Heater Power</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LB_comment</name>
    <text>To use the load connection commands, turn off PSU card </text>
    <x>290</x>
    <y>200</y>
    <width>383</width>
    <height>25</height>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="Enable" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv1 != 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv1 == 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${Index}:PSU_Status</pv_name>
      </rule>
      <rule name="visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>Not used when single load is connected</text>
    <x>20</x>
    <y>130</y>
    <width>650</width>
    <height>41</height>
    <visible>false</visible>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <transparent>false</transparent>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 != 1">
          <value>true</value>
        </exp>
        <pv_name>${SecSub}:${Dis}-${Dev}-${MainHeaterIndex}:FB_DefaultMode</pv_name>
      </rule>
    </rules>
  </widget>
</display>
